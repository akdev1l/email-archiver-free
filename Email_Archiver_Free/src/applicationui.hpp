#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>
#include <bb/cascades/Pickers/FilePicker>
#include <bb/cascades/ActionItem>
#include <bb/cascades/NavigationPane>
#include <bb/cascades/WebView>
#include <bb/cascades/SceneCover>
#include <bb/cascades/Label>
#include <bb/cascades/Page>
#include <bb/cascades/ListView>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Container>
#include <bb/cascades/ToggleButton>
#include <bb/device/BatteryInfo>
#include <bb/device/BatteryChargingState>
#include <bb/cascades/GroupDataModel>
#include <bb/system/SystemPrompt>
#include "Accounts.hpp"
#include "RegistrationHandler.hpp"

namespace bb
{
    namespace cascades
    {
        class Application;
        class LocaleHandler;
    }
}

class QTranslator;

/*!
 * @brief Application object
 *
 *
 */

class ApplicationUI : public QObject
{
    Q_OBJECT
public:
    ApplicationUI(bb::cascades::Application *app);
    virtual ~ApplicationUI() { }
    bb::cascades::ListView *m_AccountList, *m_EmailList;
    bb::pim::message::MessageService *m_MessageService;
    AccountManager *m_AccountManager;
    bb::cascades::GroupDataModel *m_DataModel;
    bb::cascades::pickers::FilePicker *m_FilePicker;
    bb::cascades::QmlDocument *m_EmailViewer, *m_EmailReader, *m_SettingsPage;
    bb::cascades::ActionItem *m_BackupAll, *m_BackupOne, *m_SortingAction;
    bb::cascades::Label *m_SubjectLabel, *m_SenderLabel, *m_ActiveText, *m_MessagesText, *m_BatteryText;
    bb::cascades::WebView *m_BodyWebView;
    bb::cascades::NavigationPane *m_root;
    bb::cascades::SceneCover *m_SceneCover;
    bb::cascades::Container *m_ActiveRoot;
    bb::cascades::QmlDocument *m_QmlActiveFrame;
    bb::device::BatteryInfo *m_Battery;
    bb::cascades::ToggleButton *m_ToggleSort;
    bb::cascades::Page *m_EmailRoot, *m_ReaderRoot, *m_SettingsRoot;
    QList<MyClassAccount> m_AccountContainer;
    MyClassAccount m_SelectedAccount;
    RegistrationHandler *registrationHandler;
    QSettings m_Settings;
    Q_INVOKABLE void changeSortingKeys(const bool &p_IsSender);
    Q_INVOKABLE void showMessage(QVariantList indexPath);
    Q_INVOKABLE void getEmails();
    Q_INVOKABLE void showEmails(QVariantList indexPath);
    Q_INVOKABLE void backUpAllEmails(const QStringList& filePath);
    Q_INVOKABLE void backUpSingleEmail(const QStringList& filePath);
    Q_INVOKABLE void setActiveFrame();
    Q_INVOKABLE void accountNumber();

public slots:
    void changeList();
    Q_INVOKABLE void batteryChange();
    Q_INVOKABLE void saveValueFor(const QString &p_Key, const bool &p_Value);
    Q_INVOKABLE bool getValueFor(const QString &p_Key, const bool &p_DefaultValue);
    Q_INVOKABLE void savePassword( const QString &p_Password );
    Q_INVOKABLE bool comparePassword( const QString &p_Password);

};

#endif /* ApplicationUI_HPP_ */
