#include <QObject>
#include <bb/pim/account/AccountService>
#include <bb/pim/account/Account>
#include <bb/pim/account/Provider>
#include <bb/pim/message/MessageService>
#include <bb/pim/message/Message>
#include <bb/pim/message/MessageFilter>
#include <bb/pim/message/MessageBody>
#include <bb/pim/message/Attachment>
#include <bb/cascades/GroupDataModel>
using namespace bb::cascades;
using namespace bb::pim::account;
using namespace bb::pim::message;

class AccountManager{
private:
	MessageService *m_MessageService;

public:
	AccountService *m_AccountService;
	const MessageFilter m_MessageFilter;
	AccountManager();
	GroupDataModel* getAccountNames();

	QList<AccountKey> getAccountIds();
	QList<MessageKey> getMessagesIds( AccountKey p_AccountId );

	QList<Account> getAccounts();
	QList<Message> getMessages( AccountKey p_AccountId );

	Account getAccount( AccountKey p_AccountId );
	Message getMessage( AccountKey p_AccountId, MessageKey p_MessageId );
};
class MyMessageClass{
public:
	QString subject;
	QString body;
	QString sender;
	MessageKey id;
	QDateTime serverTimeStamp;
	QList<Attachment> attachments;
};
class MyClassAccount{
public:
	AccountKey id;
	QString displayName;
	QList<MyMessageClass> messages;
};
