#include "Accounts.hpp"
using namespace bb::cascades;
using namespace bb::pim::account;
using namespace bb::pim::message;

AccountManager::AccountManager(){
	m_AccountService = new AccountService() ;
	m_MessageService = new MessageService() ;
}

GroupDataModel* AccountManager::getAccountNames(){
	GroupDataModel *returnDataModel = new GroupDataModel(QStringList() << "Accounts");
	QVariantMap entries;
	foreach( const Account &acc, m_AccountService->accounts()){
		entries["DisplayName"] = acc.displayName();
		entries["AccountId"] = acc.id();
		if(entries["DisplayName"]!="" && (acc.provider().name().contains("Email") || acc.provider().name().contains("ActiveSync")) ){
            //returnDataModel->insert(entries);
		}
	}
	return returnDataModel;
}

QList<AccountKey> AccountManager::getAccountIds(){
	QList<AccountKey> *idList = new QList<long long int>();
	QVariantMap entries;
	foreach( const Account &acc, m_AccountService->accounts()){
		entries["AccountIds"] = acc.id();
		if(!acc.displayName().isEmpty()){
			idList->append(acc.id());
		}
	}
	return *idList;
}

QList<MessageKey> AccountManager::getMessagesIds( AccountKey p_AccountId ){
	QList<MessageKey> *messageKeyList = new QList<MessageKey>();
	QList<Message> messageList = m_MessageService->messages( p_AccountId, m_MessageFilter );
	foreach( const Message &mess, messageList){
		messageKeyList->append(mess.id());
	}
	return *messageKeyList;
}

Account AccountManager::getAccount( AccountKey p_AccountId ){
	return m_AccountService->account( p_AccountId );
}

Message AccountManager::getMessage( AccountKey p_AccountId, MessageKey p_MessageId){
	return m_MessageService->message( p_AccountId, p_MessageId );
}

QList<Account> AccountManager::getAccounts(){
	return m_AccountService->accounts();
}

QList<Message> AccountManager::getMessages( AccountKey p_AccountId ){
	MessageFilter *m = new MessageFilter();
	m->insert(MessageFilter::Quantity, QVariant(50));
	//m->insert(MessageFilter::TransmissionStatus, QVariant("Received"));
	return m_MessageService->messages( p_AccountId, *m );
}
