#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/ListView>
#include <bb/cascades/GroupDataModel>
#include <bb/pim/account/AccountService>
#include <bb/pim/account/Account>
#include <bb/pim/account/Provider>
#include <bb/pim/message/MessageService>
#include <bb/pim/message/Message>
#include <bb/pim/message/MessageFilter>
#include <bb/pim/message/MessageBody>
#include <bb/pim/message/Attachment>
#include <bb/cascades/TabbedPane>
#include <bb/cascades/Pickers/FilePicker>
#include <bb/cascades/ActionItem>
#include <bb/cascades/Tab>
#include <bb/cascades/WebView>
#include <bb/cascades/Label>
#include "RegistrationHandler.hpp"



using namespace bb::cascades;
using namespace bb::pim::account;
using namespace bb::pim::message;
using namespace bb::cascades::pickers;
using namespace bb::device;
using namespace bb::system;

int AccountNumber = 0;
ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
        QObject(app)
{
    // Create scene document from main.qml asset, the parent is set
    // to ensure the document gets destroyed properly at shut down.
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);
    m_QmlActiveFrame = QmlDocument::create("asset:///ActiveFrame.qml").parent(this);
    m_EmailViewer = QmlDocument::create("asset:///EmailList.qml").parent(this);
	m_EmailReader = QmlDocument::create("asset:///EmailReader.qml").parent(this);
	m_SettingsPage = QmlDocument::create("asset:///SettingsPage.qml").parent(this);
	m_SettingsPage->setContextProperty("app", this);
    m_QmlActiveFrame->setContextProperty("_Active", this);
    m_EmailViewer->setContextProperty("app", this);
    m_EmailReader->setContextProperty("app", this);
    qml->setContextProperty("app", this);

    // Create root object for the UI
    m_root = qml->createRootObject<NavigationPane>();
    m_EmailRoot = m_EmailViewer->createRootObject<Page>();
    m_ReaderRoot = m_EmailReader->createRootObject<Page>();

	m_MessageService = new MessageService();
	m_AccountManager = new AccountManager();
	m_EmailList = m_EmailRoot->findChild<ListView*>("emailList");
	m_AccountList = m_root->findChild<ListView*>("accountList");
	m_FilePicker = m_root->findChild<FilePicker*>("filePick");
	m_BackupAll = m_root->findChild<ActionItem*>("backupAllItem");
	m_SenderLabel = m_ReaderRoot->findChild<Label*>("senderLabel");
	m_SubjectLabel = m_ReaderRoot->findChild<Label*>("subjectLabel");
	m_BodyWebView = m_ReaderRoot->findChild<WebView*>("bodyWebView");
	m_BackupOne = m_ReaderRoot->findChild<ActionItem*>("backupOne");
	m_SortingAction = m_EmailRoot->findChild<ActionItem*>("sortingAction");
	m_Battery = new BatteryInfo();
	m_ToggleSort = m_root->findChild<ToggleButton*>("toggleSort");
	const QUuid uuid(QLatin1String("561f378a-d0f0-486e-a4d8-994fa1513d6f"));
	registrationHandler = new RegistrationHandler(uuid, this);
	setActiveFrame();
	batteryChange();
	bool settings = getValueFor("sort", false);
	if(settings){
		m_SortingAction->setTitle("Sort by Date");
	}
	// Set created root object as the application scene
	QObject::connect(m_Battery, SIGNAL(levelChanged (int, bb::device::BatteryChargingState::Type )), this, SLOT(batteryChange()));
    app->setScene(m_root);
}
void ApplicationUI::changeList(){
	GroupDataModel *dataModel = new GroupDataModel(QStringList() << "description" << "id");
	QVariantMap tempMap;
	foreach( const MyClassAccount &tempAcc, m_AccountContainer ){
		tempMap["AccountId"] = tempAcc.id;
		tempMap["DisplayName"] = tempAcc.displayName;
		dataModel->insert(tempMap);
	}
	m_AccountList->setDataModel(dataModel);
}
void ApplicationUI::getEmails(){
	m_AccountContainer.clear();
	foreach( const Account &acc, m_AccountManager->getAccounts()){
		AccountNumber++;
		MyClassAccount *tempAccount = new MyClassAccount();
		if( !acc.displayName().isEmpty() && (acc.provider().name().contains("Email") || acc.provider().name().contains("ActiveSync")) ){
				tempAccount->displayName = acc.displayName();
				tempAccount->id = acc.id();
			foreach( const Message &mess, m_AccountManager->getMessages(acc.id())){
				MyMessageClass *tempMessage = new MyMessageClass();
				if(mess.subject().isEmpty()){
					tempMessage->subject = "No Subject";
				}
				else{
					tempMessage->subject = mess.subject();
				}
				QString tempBody = QString(mess.body(MessageBody::Html).data());
				if(!tempBody.isEmpty()){
					tempMessage->body = tempBody;
				}
				else{
					tempMessage->body = mess.body(MessageBody::PlainText).plainText();
				}
				tempMessage->sender = mess.sender().address();
				tempMessage->id = mess.id();
				if(mess.hasAttachments()){
					tempMessage->attachments = mess.attachments();
				}
				tempMessage->serverTimeStamp = mess.serverTimestamp();
				tempAccount->messages.append(*tempMessage);
			}
			m_AccountContainer.append(*tempAccount);
		}
	}
	changeList();
}
void ApplicationUI::showEmails(QVariantList indexPath){
	if(indexPath.count()<2){
		indexPath.append(QVariant(0));
	}
	m_DataModel = new GroupDataModel(QStringList() << "Subject" << "Sender" << "serverTimeStamp");
	QVariantMap tempMap;
	foreach( const MyClassAccount &acc, m_AccountContainer){
		if(acc.id == m_AccountList->dataModel()->data(indexPath).toMap()["AccountId"].toInt() ){
			m_SelectedAccount = acc;
		}
	}
	foreach( const MyMessageClass &tempMessage, m_SelectedAccount.messages){
        tempMap["Sender"] = tempMessage.subject;
        tempMap["Subject"] = tempMessage.sender;
		tempMap["MessageId"] = tempMessage.id;
		tempMap["serverTimeStamp"] = tempMessage.serverTimeStamp.toString("yyyy-MM-dd");
		if(!getValueFor("sort", false)){
			m_DataModel->setSortingKeys(QStringList("serverTimeStamp"));
		}
		else{
			m_DataModel->setSortingKeys(QStringList("Sender"));
		}
		m_DataModel->setSortedAscending(false);
		m_DataModel->insert(tempMap);
	}
	qDebug() << m_DataModel->size();
	m_EmailList->setDataModel(m_DataModel);
	m_root->push(m_EmailRoot);
}

void ApplicationUI::showMessage(QVariantList indexPath){
	if(indexPath.count() < 2){
		indexPath.append(QVariant(0));
	}
	int messageID = m_EmailList->dataModel()->data(indexPath).toMap()["MessageId"].toInt();
	MyMessageClass myTempMessage;
	foreach( const MyMessageClass &mess, m_SelectedAccount.messages ){
		if(messageID == mess.id){
			myTempMessage = mess;
		}
	}
	m_SenderLabel->setText(QString("From: ")+myTempMessage.sender);
	m_SubjectLabel->setText(QString("Subject: ")+myTempMessage.subject);
	m_BodyWebView->setHtml(myTempMessage.body);
	m_root->push(m_ReaderRoot);
}
void ApplicationUI::backUpSingleEmail(const QStringList& filePath){
	QFile *file;
	if(getValueFor("toHTML", false)){
		file = new QFile(filePath.first()+QString(".html"));
	}
	else{
		file = new QFile(filePath.first()+QString(".eml"));
	}
	file->open(QIODevice::WriteOnly);
	QByteArray tempBytes;
	QString lineBreak = "\n";
	tempBytes.append(m_SenderLabel->text()+lineBreak);
	tempBytes.append(m_SubjectLabel->text()+lineBreak);
	tempBytes.append(QString("Content-Type: text/html; charset=ISO-8859-1\n"));
	tempBytes.append(m_BodyWebView->html());
	file->write(tempBytes);
	file->close();
}
void ApplicationUI::backUpAllEmails(const QStringList& filePath){
	foreach(const MyClassAccount &tempAcc, m_AccountContainer){
		QString path = filePath.first()+"/"+tempAcc.displayName;
		QDir saveDir = QDir::root();
		saveDir.mkpath(path);
		foreach(const MyMessageClass &tempMess, tempAcc.messages){
			QString fileName = tempMess.subject;
			fileName.truncate(30);
			QFile *file;
			if(getValueFor("toHTML", false)){
				file = new QFile(path+QString("/")+fileName+QString(".html"));
			}
			else{
				file = new QFile(path+QString("/")+fileName+QString(".eml"));
			}
			file->open(QIODevice::WriteOnly);
			QByteArray tempByte;
			tempByte.append(QString("From:")+tempMess.sender+QString("\n"));
			tempByte.append(QString("Subject:")+tempMess.subject+QString("\n"));
			tempByte.append(QString("Date:")+tempMess.serverTimeStamp.toString("ddd, dd MMM yyyy hh:mm:ss")+QString("\n"));
			tempByte.append(QString("Content-Type: text/html; charset=UTF-8\n"));
			tempByte.append(tempMess.body);
			file->write(tempByte);
			file->close();
		}
	}
}
void ApplicationUI::setActiveFrame(){
	m_ActiveRoot = m_QmlActiveFrame->createRootObject<Container>();
	m_ActiveText = m_ActiveRoot->findChild<Label*>("activeText");
	m_MessagesText = m_ActiveRoot->findChild<Label*>("messagesText");
	m_BatteryText = m_ActiveRoot->findChild<Label*>("batteryText");
	m_SceneCover = SceneCover::create().content(m_ActiveRoot);
	Application::instance()->setCover(m_SceneCover);
}
void ApplicationUI::changeSortingKeys(const bool &p_IsSender){
	if(!p_IsSender){
		m_DataModel->setSortingKeys(QStringList("Sender"));
		m_DataModel->setSortedAscending(true);
	}
	else{
		m_DataModel->setSortingKeys(QStringList("serverTimeStamp"));
		m_DataModel->setSortedAscending(false);
	}
	m_EmailList->resetDataModel();
	m_EmailList->setDataModel(m_DataModel);
}
void ApplicationUI::accountNumber(){
	m_ActiveText->setText(QString("Accounts: ")+QString::number(m_AccountContainer.size()));
	int messagesNumber = 0;
	foreach(const MyClassAccount &acc, m_AccountContainer){
			messagesNumber+= acc.messages.size();
	}
	m_MessagesText->setText(QString("Emails: ")+QString::number(messagesNumber));
}
void ApplicationUI::batteryChange(){
	m_BatteryText->setText(QString::number(m_Battery->level()));
}
bool ApplicationUI::getValueFor( const QString &p_Key, const bool &p_DefaultValue){
	if(m_Settings.value(p_Key).isNull()){
		return p_DefaultValue;
	}
	else{
		return m_Settings.value(p_Key).toBool();
	}
}
void ApplicationUI::saveValueFor( const QString &p_Key, const bool &p_Value){
	m_Settings.setValue(p_Key, QVariant(p_Value));
}
void ApplicationUI::savePassword( const QString &p_Password ){
	QCryptographicHash encryptedPass(QCryptographicHash::Sha1);
	encryptedPass.addData(p_Password.toUtf8());
	m_Settings.setValue("password", QVariant(encryptedPass.result().toHex()));
}
bool ApplicationUI::comparePassword( const QString &p_Password){
	QCryptographicHash tempPass(QCryptographicHash::Sha1);
	tempPass.addData(p_Password.toUtf8());
	return QVariant(tempPass.result().toHex()) == m_Settings.value("password");
}
