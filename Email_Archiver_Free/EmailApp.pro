APP_NAME = EmailApp

CONFIG += qt warn_on cascades10
LIBS += -lbbpim
LIBS += -lbbcascadespickers
LIBS += -lbbdevice
LIBS += -lbbplatformbbm -lbbsystem

include(config.pri)
