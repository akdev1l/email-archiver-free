import bb.cascades 1.0
import bb.cascades.pickers 1.0

Page {
    actions: [
        ActionItem {
            objectName: "backupOne"
            title: "Back up!"
            imageSource: "asset:///icons/backup.png"
            onTriggered: {
                filePick.open();
            }
            ActionBar.placement: ActionBarPlacement.OnBar
        }
    ]
    Container {
        layout: DockLayout {

        }
        attachedObjects: [
            FilePicker {
                title: "Save as.."
                objectName: "filePick"
                id: filePick
                mode: FilePickerMode.Saver
                allowOverwrite: true
                onFileSelected: {
                    app.backUpSingleEmail(selectedFiles)
                }
            },
            Invocation {
                id: invokateBrowser
                query{
                    mimeType: "text/html"
                    onQueryChanged: {
                        query.updateQuery()
                    }
                }
                onArmed: {
                    if(query.uri!=""){
                        trigger("bb.action.OPEN")
                    }
                }
            }
        ]
        Container {
            Container{
                id: infoContainer
                Label {
                    objectName: "subjectLabel"
                    text: qsTr("")
                    textStyle.fontSize: FontSize.Medium
                    multiline: true
                }
                Label {
                    objectName: "senderLabel"
                    textStyle.fontSize: FontSize.Small
                    textStyle.fontWeight: FontWeight.W100
    
                }
                Divider {
                }
            }
            ScrollView {
                id: scroll
                scrollViewProperties.minContentScale: 1.0
                scrollViewProperties.maxContentScale: 4.0
                scrollViewProperties.scrollMode: ScrollMode.Both
                scrollViewProperties.pinchToZoomEnabled: true
                WebView {
                    objectName: "bodyWebView"
                    settings.javaScriptEnabled: false
                    settings.defaultTextCodecName: "UTF-8"
                    minWidth: 720.0
                    minHeight: 600.0
                    onLoadProgressChanged: {
                        progressBar.visible = true
                        progressBar.value = loadProgress
                        progressBar.toValue = loadProgress.MAX_VALUE
                        progressBar.fromValue = loadProgress.MIN_VALUE
                    }
                    onLoadingChanged: {
                        if (loadRequest.status == WebLoadStatus.Succeeded) {
                            progressBar.visible = false
                        }
                    }
                    onNavigationRequested: {
                        console.log("NavigationRequested: " + request.url + " navigationType=" + request.navigationType)
                        if (request.url != "local:///") {
                            request.action = WebNavigationRequestAction.Ignore
                            invokateBrowser.query.uri = request.url

                        }
                    }
                }
            }
        }
        ProgressIndicator {
            fromValue: 0.0
            toValue: 100.0
            id: progressBar
            preferredWidth: 768.0
            minWidth: 720.0
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Bottom

        }
    }
}
