import bb.cascades 1.0

Page {
    property alias sortingAction: sortingAction
    actions: [
        ActionItem {
            title: "Sort by Sender"
            id: sortingAction
            objectName: "sortingAction"
            imageSource:  if (title == "Sort by Sender") {
           "asset:///icons/sortbysender.png"
            } else {
                "asset:///icons/sortbydate.png"
            }
            onTriggered: {
                if (title == "Sort by Sender") {
                    app.changeSortingKeys(false)
                    title = "Sort by Date"
                } else {
                    app.changeSortingKeys(true)
                    title = "Sort by Sender"
                }

            }
            ActionBar.placement: ActionBarPlacement.InOverflow
        }
    ]
    titleBar: TitleBar {
        title: "Emails"
    }
    Container {
        ListView {
            id: emailList
            objectName: "emailList"
            onTriggered: {
                app.showMessage(indexPath)
            }
            listItemComponents: [
                ListItemComponent {
                    type: "header"
                    Header {
                        title: ListItemData
                    }
                },
                ListItemComponent {
                    type: "item"
                    Container {
                        background: Color.White
                        layout: DockLayout {

                        }
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                        Container {
                            verticalAlignment: VerticalAlignment.Fill
                            horizontalAlignment: HorizontalAlignment.Fill
                            Container {
                                horizontalAlignment: HorizontalAlignment.Fill
//                                background: Color.create("#ffe7e7e7")
                                Container {
                                    verticalAlignment: VerticalAlignment.Fill
                                    horizontalAlignment: HorizontalAlignment.Fill
                                    topPadding: 8.0
                                    bottomPadding: 8.0
                                    leftPadding: 12.0
                                    rightPadding: 12.0
                                    Label {
                                        text: ListItemData.Sender
                                        textStyle.fontSize: FontSize.Medium
                                        textStyle.fontWeight: FontWeight.W500
                                        //                                       textStyle.fontStyle: FontStyle.Italic
                                    }
                                }
                            }
                            Container {
                                verticalAlignment: VerticalAlignment.Fill
                                horizontalAlignment: HorizontalAlignment.Fill
                                layout: DockLayout {

                                }
                                leftPadding: 15.0
                                rightPadding: 12.0
                                topPadding: 5.0
                                bottomPadding: 5.0
                                Label {
                                    text: ListItemData.Subject
                                    multiline: true
                                    textStyle.fontSize: FontSize.Small
                                    autoSize.maxLineCount: 5
                                    textStyle.fontWeight: FontWeight.W400
                                }
                            }
                            Divider {
                            }
                        }
                    }
                }
            ]
        }
    }
}
