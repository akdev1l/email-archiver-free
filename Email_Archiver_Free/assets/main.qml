import bb.cascades 1.0
import bb.cascades.pickers 1.0
import bb.system 1.0


NavigationPane {
    id: navPane
    Menu.definition: [
        MenuDefinition {
            actions: [
                ActionItem {
                    title: "Contact us!"
                    imageSource: "asset:///icons/ic_info.png"
                    attachedObjects: [
                        Invocation {
                            id: invoke
                            query.mimeType: ""
                            query.uri: "mailto:support@rpfapps.com?subject=Email%20Archiver%20Free:%20Support"
                            query.invokeTargetId: "sys.pim.uib.email.hybridcomposer"
                        }
                    ]
                    onTriggered: {
                        invoke.trigger("bb.action.SENDEMAIL")
                    }
                },
                ActionItem {
                    title: "Settings"
                    onTriggered: {
                        navPane.push(settingsDefinition);
                    }
                },
                ActionItem {
                    title: "Full version!"
                    imageSource: "asset:///icons/buy.png"
                    attachedObjects: [
                        Invocation {
                            id: invokeAppworld
                            query.mimeType: ""                            
                            query.uri: "appworld://content/33549889"
                            query.invokeTargetId: "sys.appworld"
                        }
                    ]
                    onTriggered: {
                        invokeAppworld.trigger("bb.action.OPEN")
                    }
                }
            ]
        }
    ]
    attachedObjects: [
        FilePicker {
            title: "Save as.."
            objectName: "filePick"
            id: filePick
            mode: FilePickerMode.Saver
            allowOverwrite: true
            onFileSelected: {
                app.backUpSingleEmail(selectedFiles)
            }
        },
        SettingsPage {
            id: settingsDefinition
        },
        EmailList {
            id: emailListDef 
        },
        EmailReader {
            id: emailReaderDef
        },
        SystemToast {
            id: backedUp
            body: "Emails saved to"
        },
        FilePicker {
            id: backUpAllPick
            mode: FilePickerMode.SaverMultiple
            onFileSelected: {
                app.backUpAllEmails(selectedFiles)
                backedUp.body = "Emails Saved to " + selectedFiles
                backedUp.show()
            }
            type: FileType.Other
        }
    ]
    onCreationCompleted: {
       _registrationHandler.registerApplication();
        acIndicator.visible = true;
    }
    Page {
        actions: [
            ActionItem {
                title: "Back up All!"
                id: backupAll
                objectName: "backupAllItem"
                imageSource: "asset:///icons/backup.png"
                onTriggered: {
                    backUpAllPick.open()
                }
                ActionBar.placement: ActionBarPlacement.OnBar
            }
        ]
        titleBar: TitleBar {
            id: accountTitle
            title: "Accounts"

        }
        Container {
            layout: DockLayout {
            
            }
            Container {
                id: acContainer
                layout: StackLayout {
                
                }
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                Container {
                    layout: DockLayout {
                    
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    ActivityIndicator {
                        preferredWidth: 300.0
                        preferredHeight: 300.0
                        id: acIndicator
                        running: true
                        onStarted: {
                            backupAll.enabled = false
                            app.getEmails()
                            acIndicator.stop()
                            backupAll.enabled = true
                            acContainer.visible = false
                        }
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                }
                Container {
                    horizontalAlignment: HorizontalAlignment.Center
                    layout: DockLayout {
                    
                    }
                    Label {
                        text: "Loading emails..."
                        horizontalAlignment: HorizontalAlignment.Center
                        textStyle.color: Color.Gray
                        verticalAlignment: VerticalAlignment.Top
                    }
                }
            }
            ListView {
                onTriggered: {
                    app.showEmails(indexPath)
                }
                objectName: "accountList"
                listItemComponents: [
                    ListItemComponent {
                        type: "header"
                        Header {
                            title: "Email Accounts"
                            horizontalAlignment: HorizontalAlignment.Right
                        }
                    },
                    ListItemComponent {
                        type: "item"
                        Container {
                           background: Color.White
                           Container {
                                topPadding: 18.0
                                bottomPadding: 2.0
                                leftPadding: 15.0
                                rightPadding: 15.0
                                Label {
                                   text: ListItemData.DisplayName
                                    textStyle.fontSize: FontSize.Large

                                }
                               Label {
                                   visible: false
                                   text: ListItemData.AccountId
                               }
                           }
                            Divider {
                            }
                        }
                    }
                ]
            }
        }
    }

}
