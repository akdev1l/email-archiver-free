import bb.cascades 1.0
import bb.system 1.0

Page {
    attachedObjects: [
        SystemPrompt {
            id: configPrompt
            objectName: "sysPrompt"
            title: "Insert password:"
            body: "Insert password:"
            onFinished: {
                if(value == 2){
                    app.savePassword(inputFieldTextEntry());
                }
            }
        },
        SystemPrompt {
            id: authPrompt
            title: "Insert password:"
            body: "Insert password:"
        }
    ]
    titleBar: TitleBar {
        title: "Settings"
    }
    Container {
        layout: DockLayout {
        
        }
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill
        Container {
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            Divider {}
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                
                }
                topPadding: 18.0
                leftPadding: 12.0
                rightPadding: 12.0
                Label {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 4.0
                    }
                    text: "Sort by Sender"
                }
                ToggleButton {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 2.0
                    }
                    onCreationCompleted: { 
                        checked = app.getValueFor("sort", false)                          
                    }
                    id: sortingChecked
                    objectName: "toggleSort"
                    onCheckedChanged: {
                        app.saveValueFor("sort", checked)
                    }
                }
            }
            Divider {
            }
            Container{
                topPadding: 18.0
                leftPadding: 12.0
                rightPadding: 12.0
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight

                }
                Label {
                    text: "Password"
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 4.0
                    }
                    verticalAlignment: VerticalAlignment.Center
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom

                    }
                    ToggleButton {
                        enabled: false
                        onCreationCompleted: {
                            checked = false
                            configPrompt.inputField.inputMode = 2
                            authPrompt.inputField.inputMode = 2
                            if(checked){
                                while(!app.comparePassword(authPrompt.inputFieldTextEntry())){
                                    authPrompt.exec()
                                }
                            }
                        }
                        onCheckedChanged: {
                            app.saveValueFor("setPassword", checked)
                            if(setPass.enabled && checked){
                                setPass.clicked();
                            }
                        }
                        horizontalAlignment: HorizontalAlignment.Right
                    }
                    Button {
                        id: setPass
                        text: "Set Password"
                        horizontalAlignment: HorizontalAlignment.Right
                        enabled: false
                        onClicked: {
                            configPrompt.show()
                        }
                    }
                }
            }
            Divider {
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight

                }
                rightPadding: 12.0
                leftPadding: 12.0
                Label {
                    text: "Save to HTML"
                    opacity: 1.0
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 4.0

                    }
                }
                ToggleButton {
                    enabled: false
                    onCreationCompleted: {
                        checked = app.getValueFor("toHTML", false)
                    }
                    onCheckedChanged: {
                        app.saveValueFor("toHTML", checked)
                    }
                }
            }
            Divider {
                
            }
        }
        Container {
            bottomPadding: 35.0
            
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Center
            Label {
                text: "Changes take effect after restarting the app"
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontStyle: FontStyle.Italic
            }
        }
    }
}

